package com.fts.rpc;

import static io.grpc.stub.ClientCalls.asyncUnaryCall;
import static io.grpc.stub.ClientCalls.asyncServerStreamingCall;
import static io.grpc.stub.ClientCalls.asyncClientStreamingCall;
import static io.grpc.stub.ClientCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ClientCalls.blockingUnaryCall;
import static io.grpc.stub.ClientCalls.blockingServerStreamingCall;
import static io.grpc.stub.ClientCalls.futureUnaryCall;
import static io.grpc.MethodDescriptor.generateFullMethodName;
import static io.grpc.stub.ServerCalls.asyncUnaryCall;
import static io.grpc.stub.ServerCalls.asyncServerStreamingCall;
import static io.grpc.stub.ServerCalls.asyncClientStreamingCall;
import static io.grpc.stub.ServerCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedStreamingCall;

/**
 */
@javax.annotation.Generated(
    value = "by gRPC proto compiler (version 1.6.1)",
    comments = "Source: opencv.proto")
public final class ImageServicesGrpc {

  private ImageServicesGrpc() {}

  public static final String SERVICE_NAME = "rpc.ImageServices";

  // Static method descriptors that strictly reflect the proto.
  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  public static final io.grpc.MethodDescriptor<com.fts.rpc.Opencv.Image,
      com.fts.rpc.Opencv.MotionDetectResult> METHOD_MOTION_DETECTION =
      io.grpc.MethodDescriptor.<com.fts.rpc.Opencv.Image, com.fts.rpc.Opencv.MotionDetectResult>newBuilder()
          .setType(io.grpc.MethodDescriptor.MethodType.BIDI_STREAMING)
          .setFullMethodName(generateFullMethodName(
              "rpc.ImageServices", "motionDetection"))
          .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
              com.fts.rpc.Opencv.Image.getDefaultInstance()))
          .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
              com.fts.rpc.Opencv.MotionDetectResult.getDefaultInstance()))
          .build();
  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  public static final io.grpc.MethodDescriptor<com.fts.rpc.Opencv.Image,
      com.fts.rpc.Opencv.FaceDetectResult> METHOD_FACE_DETECTION =
      io.grpc.MethodDescriptor.<com.fts.rpc.Opencv.Image, com.fts.rpc.Opencv.FaceDetectResult>newBuilder()
          .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
          .setFullMethodName(generateFullMethodName(
              "rpc.ImageServices", "faceDetection"))
          .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
              com.fts.rpc.Opencv.Image.getDefaultInstance()))
          .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
              com.fts.rpc.Opencv.FaceDetectResult.getDefaultInstance()))
          .build();

  /**
   * Creates a new async stub that supports all call types for the service
   */
  public static ImageServicesStub newStub(io.grpc.Channel channel) {
    return new ImageServicesStub(channel);
  }

  /**
   * Creates a new blocking-style stub that supports unary and streaming output calls on the service
   */
  public static ImageServicesBlockingStub newBlockingStub(
      io.grpc.Channel channel) {
    return new ImageServicesBlockingStub(channel);
  }

  /**
   * Creates a new ListenableFuture-style stub that supports unary calls on the service
   */
  public static ImageServicesFutureStub newFutureStub(
      io.grpc.Channel channel) {
    return new ImageServicesFutureStub(channel);
  }

  /**
   */
  public static abstract class ImageServicesImplBase implements io.grpc.BindableService {

    /**
     */
    public io.grpc.stub.StreamObserver<com.fts.rpc.Opencv.Image> motionDetection(
        io.grpc.stub.StreamObserver<com.fts.rpc.Opencv.MotionDetectResult> responseObserver) {
      return asyncUnimplementedStreamingCall(METHOD_MOTION_DETECTION, responseObserver);
    }

    /**
     */
    public void faceDetection(com.fts.rpc.Opencv.Image request,
        io.grpc.stub.StreamObserver<com.fts.rpc.Opencv.FaceDetectResult> responseObserver) {
      asyncUnimplementedUnaryCall(METHOD_FACE_DETECTION, responseObserver);
    }

    @java.lang.Override public final io.grpc.ServerServiceDefinition bindService() {
      return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
          .addMethod(
            METHOD_MOTION_DETECTION,
            asyncBidiStreamingCall(
              new MethodHandlers<
                com.fts.rpc.Opencv.Image,
                com.fts.rpc.Opencv.MotionDetectResult>(
                  this, METHODID_MOTION_DETECTION)))
          .addMethod(
            METHOD_FACE_DETECTION,
            asyncUnaryCall(
              new MethodHandlers<
                com.fts.rpc.Opencv.Image,
                com.fts.rpc.Opencv.FaceDetectResult>(
                  this, METHODID_FACE_DETECTION)))
          .build();
    }
  }

  /**
   */
  public static final class ImageServicesStub extends io.grpc.stub.AbstractStub<ImageServicesStub> {
    private ImageServicesStub(io.grpc.Channel channel) {
      super(channel);
    }

    private ImageServicesStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected ImageServicesStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new ImageServicesStub(channel, callOptions);
    }

    /**
     */
    public io.grpc.stub.StreamObserver<com.fts.rpc.Opencv.Image> motionDetection(
        io.grpc.stub.StreamObserver<com.fts.rpc.Opencv.MotionDetectResult> responseObserver) {
      return asyncBidiStreamingCall(
          getChannel().newCall(METHOD_MOTION_DETECTION, getCallOptions()), responseObserver);
    }

    /**
     */
    public void faceDetection(com.fts.rpc.Opencv.Image request,
        io.grpc.stub.StreamObserver<com.fts.rpc.Opencv.FaceDetectResult> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(METHOD_FACE_DETECTION, getCallOptions()), request, responseObserver);
    }
  }

  /**
   */
  public static final class ImageServicesBlockingStub extends io.grpc.stub.AbstractStub<ImageServicesBlockingStub> {
    private ImageServicesBlockingStub(io.grpc.Channel channel) {
      super(channel);
    }

    private ImageServicesBlockingStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected ImageServicesBlockingStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new ImageServicesBlockingStub(channel, callOptions);
    }

    /**
     */
    public com.fts.rpc.Opencv.FaceDetectResult faceDetection(com.fts.rpc.Opencv.Image request) {
      return blockingUnaryCall(
          getChannel(), METHOD_FACE_DETECTION, getCallOptions(), request);
    }
  }

  /**
   */
  public static final class ImageServicesFutureStub extends io.grpc.stub.AbstractStub<ImageServicesFutureStub> {
    private ImageServicesFutureStub(io.grpc.Channel channel) {
      super(channel);
    }

    private ImageServicesFutureStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected ImageServicesFutureStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new ImageServicesFutureStub(channel, callOptions);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<com.fts.rpc.Opencv.FaceDetectResult> faceDetection(
        com.fts.rpc.Opencv.Image request) {
      return futureUnaryCall(
          getChannel().newCall(METHOD_FACE_DETECTION, getCallOptions()), request);
    }
  }

  private static final int METHODID_FACE_DETECTION = 0;
  private static final int METHODID_MOTION_DETECTION = 1;

  private static final class MethodHandlers<Req, Resp> implements
      io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
    private final ImageServicesImplBase serviceImpl;
    private final int methodId;

    MethodHandlers(ImageServicesImplBase serviceImpl, int methodId) {
      this.serviceImpl = serviceImpl;
      this.methodId = methodId;
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_FACE_DETECTION:
          serviceImpl.faceDetection((com.fts.rpc.Opencv.Image) request,
              (io.grpc.stub.StreamObserver<com.fts.rpc.Opencv.FaceDetectResult>) responseObserver);
          break;
        default:
          throw new AssertionError();
      }
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public io.grpc.stub.StreamObserver<Req> invoke(
        io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_MOTION_DETECTION:
          return (io.grpc.stub.StreamObserver<Req>) serviceImpl.motionDetection(
              (io.grpc.stub.StreamObserver<com.fts.rpc.Opencv.MotionDetectResult>) responseObserver);
        default:
          throw new AssertionError();
      }
    }
  }

  private static final class ImageServicesDescriptorSupplier implements io.grpc.protobuf.ProtoFileDescriptorSupplier {
    @java.lang.Override
    public com.google.protobuf.Descriptors.FileDescriptor getFileDescriptor() {
      return com.fts.rpc.Opencv.getDescriptor();
    }
  }

  private static volatile io.grpc.ServiceDescriptor serviceDescriptor;

  public static io.grpc.ServiceDescriptor getServiceDescriptor() {
    io.grpc.ServiceDescriptor result = serviceDescriptor;
    if (result == null) {
      synchronized (ImageServicesGrpc.class) {
        result = serviceDescriptor;
        if (result == null) {
          serviceDescriptor = result = io.grpc.ServiceDescriptor.newBuilder(SERVICE_NAME)
              .setSchemaDescriptor(new ImageServicesDescriptorSupplier())
              .addMethod(METHOD_MOTION_DETECTION)
              .addMethod(METHOD_FACE_DETECTION)
              .build();
        }
      }
    }
    return result;
  }
}
